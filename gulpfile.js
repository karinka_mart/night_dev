var
        gulp = require('gulp'),
        tinypng = require('gulp-tinypng-compress');


// Оптимизировать PNG/JPG изображения для WEB
gulp.task('tinypng', function () {
    var conf = {
        key: 'b8jpXw75vG0i-1QSqWr5ZKzDrRIZGLes',
        sigFile: './public_html/images/.tinypng-sigs',
        sameDest: true,
        log: true
    };
    gulp.src('./public_html/images/*')
            .pipe(tinypng(conf))
            .pipe(gulp.dest('./public_html/images/'));
});

gulp.task('default', function () {
    // place code for your default task here
});
