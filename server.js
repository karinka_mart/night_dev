const http = require('http');
const url = require('url');

const hostname = '127.0.0.1';
const port = 3000;

const server = http.createServer((req, res) => {
    //console.log(req);
    var page = url.parse(req.url).pathname;

    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/html');

    res.write('<!DOCTYPE html>' +
            '<html>' +
            ' <head>' +
            ' <meta charset="utf-8" />' +
            ' <title>My Node.js page!</title>' +
            ' </head>' +
            ' <body>' +
            ' <p>Here is a paragraph of <strong>HTML</strong>!</p>' +
            ' <p>URL: ' + page + '</p>' +
            ' </body>' +
            '</html>');
    res.end('Hello World\n');
    res.end();

});

server.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}/`);
});
