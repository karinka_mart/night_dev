$(document).ready(function () {

    App.Users.init();

});

var App = window.App || {};

App.Users = {

    // Обходим Пользователей и создаем таблицу с данными
    _generateTableData: function () {
        var d = App.Users.data;

        var html = '';
        $.each(d.users, function (user_id, user_name) {
            html += '<tr data-user="' + user_id + '">';
            html += '<td>' + user_name + '</td>';
            html += '<td class="posts"></td>';
            html += '<td>Remove</td>';
            html += '</tr>';
        });
        $('#tableData').html(html);
    },

    // Перебираем масив с постами, идет привязка к пользователю
    _SetUserPost: function () {
        $.each(App.Users.data.posts, function (i, data) {
            var uid = data.user_id;

            $('[data-user="' + uid + '"] .posts').append(data.title + '<br />');
        });
    },

    // Запуск наполнения данных в таблицу
    _Run: function () {
        this._generateTableData();
        this._SetUserPost();
    },

    // Инициализация
    init: function () {
        // Делаем аякс запрос для получения JSON
        $.ajax({
            url: "http://karinka.img2txt.com/demo.php",
            dataType: 'json'
        }).done(function (data) {
            // Запрос успешно отработал
            App.Users.data = data; // СОхраняем данные c JSON в переменную
            App.Users._Run(); // Запускаем обработку таблицы
        }).fail(function (data) {
            // Выводим ошибку если что-то пошло не так
            $('#tableData td').text('Ошибка при загрузке данных по URL');
        });
    }
};
